import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class Location extends Component {
  constructor(props) {
    super(props);
    console.log(props);
  }
  render() {
    return (
      <div>
        <h3>Velg sted</h3>
        <Link to="Oslo">Oslo</Link>
        <Link to="Bergen">Bergen</Link>
        <input list="locations" />

        <datalist id="locations">
          <option value="test" />
          <option value="Ok" />
        </datalist>
      </div>
    );
  }
}
