import React from "react";

let max = 10;

export default ({ movies, selectMovie }) => {
  max = Math.max.apply(
    Math,
    movies.filter(movie => movie.poll_data).map(movie => movie.poll_data.Total)
  );

  return (
    <ul className="movieCloud">
      {movies.map(movie => (
        <MovieInCloud
          key={movie.movieId}
          movie={movie}
          selectMovie={selectMovie}
        />
      ))}
    </ul>
  );
};

const MovieInCloud = ({ movie, selectMovie }) => {
  const votes = movie.poll_data ? movie.poll_data.Total + 8 : 8;
  const color = `rgb(${(votes / max) * 255}, 0,0`;
  const fontSize = (votes / max) * 1.5 + 0.6;

  return (
    <li
      onClick={() => selectMovie(movie)}
      style={{ color: color, fontSize: fontSize + "rem" }}
    >
      {movie.movieTitleAllVersions}
    </li>
  );
};
