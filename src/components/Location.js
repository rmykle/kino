import React, { Component } from "react";
import axios from "axios";
import { currentDateFormatted } from "../util/DateFormat";
import MovieList from "./MovieList";
import MovieCloud from "./MovieCloud";
import { Route, Link } from "react-router-dom";
import SelectedMovie from "./SelectedMovie";
import Loader from "./Loader";

export default class Location extends Component {
  constructor(props) {
    super(props);
    this.state = {
      movies: undefined,
      isLoading: false,
      loadingComplete: false,
      categorized: false,
      date: currentDateFormatted(),
      location: this.props.match.params.location,
      selectedMovie: undefined
    };
  }

  componentDidMount() {
    if (!this.state.movies) {
      const { location, date } = this.state;
      this.setState({ isLoading: true });
      axios
        .get(`http://localhost:3000/movies/${date}/${location}`)
        .then(({ data }) => {
          this.setState({
            movies: data,
            isLoading: false,
            loadingComplete: true
          });
        })
        .catch(error => {
          this.setState({ isLoading: false, loadingComplete: true });
        });
    }
  }

  render() {
    const { location, movies } = this.state;
    const { match } = this.props;

    if (!movies) {
      return this.info();
    }
    return (
      <div>
        <h3>{location}</h3>
        <div>
          <Link to={`${match.url}`}>Hjem</Link>
          <Link to={`${match.url}/movies`}>Alle filmer</Link>
          <Link to={`${match.url}/theaters`}>Etter kino</Link>
        </div>
        <Route
          exact
          path={`${match.url}/`}
          render={() => (
            <MovieCloud
              movies={movies}
              selectMovie={movie => this.setState({ selectedMovie: movie })}
            />
          )}
        />
        <Route
          exact
          path={`${match.url}/movies`}
          render={() => (
            <MovieList
              movies={movies}
              selectMovie={movie => this.setState({ selectedMovie: movie })}
            />
          )}
        />
        {this.state.selectedMovie ? (
          <SelectedMovie
            movie={this.state.selectedMovie}
            closeModal={() => this.setState({ selectedMovie: undefined })}
          />
        ) : null}
      </div>
    );
  }

  info() {
    const { location, isLoading, loadingComplete } = this.state;

    return (
      <div>
        <h3>{location}</h3>
        {isLoading ? <Loader /> : null}
        {!isLoading && loadingComplete ? <p>Noe gikk galt :(</p> : null}
      </div>
    );
  }
}
