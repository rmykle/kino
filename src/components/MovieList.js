import React, { Component } from "react";
import MovieThumbnail from "./MovieThumbnail";

const SHOW_ALL = "";
const GROUP_BY_THEATRE = "theatre";

export default class MovieList extends Component {
  constructor(props) {
    super(props);
    this.state = { categorizeKey: SHOW_ALL };
  }
  render() {
    const { movies } = this.props;
    return (
      <div>
        <div className="categoryOption">
          <p onClick={() => this.setState({ categorize: SHOW_ALL })}>
            Vis alle
          </p>
          <p onClick={() => this.setState({ categorize: GROUP_BY_THEATRE })}>
            Gruppert
          </p>
        </div>
        <ul className="movieList">
          {!this.state.categorize
            ? movies.map(movie => (
                <MovieThumbnail
                  key={movie.articleId}
                  movie={movie}
                  selectMovie={this.props.selectMovie}
                />
              ))
            : this.categorize()}
        </ul>
      </div>
    );
  }

  categorize() {
    const theatres = {};
    for (let key in this.props.movies) {
      const movie = this.props.movies[key];

      for (let showKey in movie.shows) {
        const show = movie.shows[showKey];
        if (!theatres[show.theater]) theatres[show.theater] = [];
        theatres[show.theater].push(movie);
      }
    }

    return (
      <ul>
        {Object.keys(theatres).map(theatre => {
          return <ul key={theatre}>hei</ul>;
        })}
      </ul>
    );
  }
}
