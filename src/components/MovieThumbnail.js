import React from "react";

export default ({ movie, selectMovie }) => {
  return (
    <li onClick={() => selectMovie(movie)}>
      <img srcSet={movie.respImgParams} alt="movieposter" />
      <span> {movie.movieTitleAllVersions}</span>
    </li>
  );
};
