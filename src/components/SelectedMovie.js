import React, { Component } from "react";
import Loader from "./Loader";
import axios from "axios";

export default class SelectedMovie extends Component {
  constructor(props) {
    super(props);
    this.state = { data: undefined, isLoading: false, showDescription: false };
    this.liIndex = 0;
  }

  componentDidMount() {
    this.setState({ isLoading: true });
    axios
      .get(`http://localhost:3000/details/${this.props.movie.articleId}`)
      .then(response =>
        this.setState({
          data: { ...response.data, ...this.props.movie },
          isLoading: false
        })
      );
  }

  render() {
    const { data, isLoading } = this.state;
    const content = !data ? (
      isLoading ? (
        <Loader />
      ) : (
        "Noe gikk galt :/"
      )
    ) : (
      this.movieDetails()
    );

    console.log(this.state.data);

    return (
      <div className="modal">
        <div>
          <span className="closeModal" onClick={this.props.closeModal}>
            X
          </span>
          {content}
        </div>
      </div>
    );
  }

  movieDetails() {
    const movie = this.state.data;
    const { Sjanger } = movie.facts;

    console.log(movie);

    return (
      <>
        <h1>{movie.movieTitleAllVersions}</h1>
        <div className="img">
          <img srcSet={movie.respImgParams} alt="imageposter" />
        </div>

        <ul className="facts">
          {movie.lengde ? <li>{movie.lengde}</li> : null}
          {movie.facts.nasjonalitet ? (
            <li>{movie.facts.nasjonalitet}</li>
          ) : null}
          {movie.sensur ? <li>{movie.sensur}s aldergrense</li> : null}
          {Sjanger ? (
            <li>
              {typeof Sjanger === "string" ? Sjanger : Sjanger.join(", ")}
            </li>
          ) : null}
          <li>
            Publikums vurdering <br />{" "}
            {Math.round(movie.poll_data.Average * 10) / 10} -{" "}
            {movie.poll_data.Total} stemmer
          </li>
        </ul>
        <ul className="reviews">
          {movie.reviews.map(review => (
            <li key={this.liIndex++}>
              <div className="dice">{review.rating}</div>
              {review.reviewer}
            </li>
          ))}
        </ul>

        <div className="shows">
          <h5>Forestillinger</h5>
          {movie.shows.map(show => {
            return (
              <a key={show.showId} href={show.buyUrl}>
                {show.is3d ? "3D" : "2D"}
                <span>
                  {show.showStart} - {show.showStop}
                </span>
                <span>{show.seatsLeft} seter</span>
                <span>{show.screen}</span>
              </a>
            );
          })}
        </div>
        <div className="description">
          <h5>Beskrivelse</h5>

          {movie.description.map(desc => (
            <p key={this.liIndex++}>{desc}</p>
          ))}
        </div>
      </>
    );
  }
}
