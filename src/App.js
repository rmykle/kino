import React, { Component } from "react";

import "./App.scss";

import { BrowserRouter as Router, Route } from "react-router-dom";
import Locations from "./components/Locations";
import Location from "./components/Location";
import Header from "./components/Header";

class App extends Component {
  componentDidMount() {
    //axios.get("locations").then(({ data }) => console.log(data));
    //console.log(fetch("locations"));
  }

  render() {
    return (
      <Router>
        <div>
          <Header />
          <Route exact path="/" component={Locations} />
          <Route path="/:location" component={Location} />
        </div>
      </Router>
    );
  }
}

export default App;
