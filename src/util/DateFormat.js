export const formatDate = date => {
  return date.toLocaleDateString();
};

export const currentDateFormatted = () => {
  return formatDate(new Date());
};
