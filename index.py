from flask import Flask, jsonify, abort
from flask_cors import CORS
from flask_caching import Cache
import requests
from bs4 import BeautifulSoup
import json
from os import path

app = Flask(__name__)
CORS(app)
cache = Cache(app, config={'CACHE_TYPE': 'simple'})


url_root = "https://www.filmweb.no/template_v2/ajax/"


@app.route("/")
def index():
    return "index"


@app.route("/locations/")
def locations():
    response = requests.get(url_root +
                            "json_locations_simple.jsp")
    return jsonify(response.json())


@app.route("/movies/<date>/<location>")
@cache.cached(timeout=1200)
def movies(date, location):

    file_path = "json/{}{}.json".format(location, date)
    if path.exists(file_path):
        print("triggered")
        return read_file(file_path)

    if date is None and location is None:
        return abort(404)
    response = requests.get(
        url_root + "json_program.jsp?location={}&date={}".format(location, date))

    try:
        json_data = response.json()
        movies = json_data["days"][0]["movies"]

        poll_data = []
        movie_ids = list(
            map(lambda movie: movie["movieId"][0:3]+"|" + movie["movieId"][3:], movies))
        while len(movie_ids) > 0:
            sublist = movie_ids[:10]
            del movie_ids[:10]
            id_string = ",".join(sublist)
            response = requests.get(
                url_root+"json_pollList.jsp?articleIds={}".format(id_string)).json()
            poll_data += response

        for poll_response in poll_data:
            poll_id = poll_response["ExtId"].replace("|", "")
            for find_movie in movies:
                if find_movie["movieId"] == poll_id:
                    find_movie["poll_data"] = poll_response
                    break

           # print(matching_movies)
        with open(file_path, "w") as file:
            file.write(json.dumps(movies))

        return jsonify(movies)

    except Exception as e:
        return jsonify(e)


@app.route("/details/<movie>")
def details(movie):

    file_path = "json/{}.json".format(movie)

    if(path.exists(file_path)):
        return read_file(file_path)

    url = "https://www.filmweb.no/film/article{}.ece".format(movie)
    page = BeautifulSoup(requests.get(url).text, "html.parser")

    description = list(
        map(lambda p: p.get_text(), page.find(id="mainText").find_all("p")))

    facts = page.find(id="facts").find_all("dl")[0].find_all("div")
    facts_obj = {}
    for fact_node in facts:
        nodes = [data.get_text() for data in fact_node.find_all("dd")]
        facts_obj[fact_node.find_all("dt")[0].get_text()] = nodes[0] if len(
            nodes) == 1 else nodes

    reviews_element = page.find_all(class_="reviews")[0]
    reviews = reviews_element.find_all("a")

    review_list = []
    for review in reviews:
        review_obj = {}
        review_obj["rating"] = "".join(
            filter(lambda x: x.isdigit(), "".join(review["class"])))
        review_obj["link"] = review["href"]
        review_obj["reviewer"] = "".join(review.get_text().split())
        review_list.append(review_obj)
    details = {"reviews": review_list,
               "description": description, "facts": facts_obj}
    with open(file_path, "w") as file:
        file.write(json.dumps(details))

    return jsonify(details)


def read_file(path):
    with open(path, "r") as read_file:
        parsed = json.load(read_file)
        return jsonify(parsed)
